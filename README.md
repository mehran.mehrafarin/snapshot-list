# SnapshotList Implementation

There are two implementations for SnapshotList interface.

###1. SynchronizedUnbalancedSnapshotListMemento:
It uses memento design pattern to save the state of the `List`. It uses a `TreeMap` behind scene to save the states. The key of this `TreeMap` keeps the versions of the snapshots. Whenever `getAtVersion()` is called, it retrieves the specific version of the snapshots.


I. Pros: Retrieving the specific snapshot is so fast because it uses `TreeMap` to retrieve it.

II. Cons: It uses more resources because it keeps whole state of the `List` therefore there are lots of redundancy (duplicated data).

#### Example:

<pre>list = []        treeMap = {}</pre>

step 1: call `add(11)`

<pre>list = [11]        treeMap = {}</pre>

step 2: call `add(13)`

<pre>list = [11, 13]        treeMap = {}</pre>

step 3: call `add(17)`

<pre>list = [11, 13, 17]        treeMap = {}</pre>

step 4: call `snapshot()`

<pre>list = [11, 13, 17]        treeMap = {1=[11, 13, 17]}</pre>

<pre>return:    1</pre>

step 5: call `remove(11)`

<pre>list = [13, 17]        treeMap = {1=[11, 13, 17]}</pre>

step 6: call `snapshot()`

<pre>list = [13, 17]        treeMap = {1=[11, 13, 17], 2=[13, 17]}</pre>

<pre>return:    2</pre>

step 7: call `getAtVersion(0, 1)`

<pre>list = [13, 17]        treeMap = {1=[11, 13, 17], 2=[13, 17]}      listV1 = [11, 13, 17]</pre>

<pre>return:    listV1[0] = 11</pre>

###2. SynchronizedUnbalancedSnapshotList: 
It uses command design pattern behind the scene, and it has its own pros and cons. It saves the changes inside the a `Deque`.
when `snapshot()` method is called, it adds the tag in this `Deque` and it returns the `version`. It gives the ability to get the specific snapshot based on the `version`. When `getAtVersion()` is called it begins to create the `List` based on these changes, which is saved inside the `Deque`.

I. Pros: It uses less resources. Because it doesn't save the whole state of the object (it just save the changes).

II. Cons: Get the specific version can be slower than `SynchronizedUnbalancedSnapshotListMemento` implementation because it has to retrieve all the changes in order to get the specific state of the list.

#### Example:

<pre>list = []        queue = []</pre>

step 1: call `add(11)` 

<pre>list = [11]        queue = [add(11)]</pre>

step 2: call `add(13)`

<pre>list = [11, 13]        queue = [add(11), add(13)]</pre>

step 3: call `add(17)`

<pre>list = [11, 13, 17]        queue = [add(11), add(13), add(17)]</pre>

step 4: call `snapshot()`

<pre>list = [11, 13, 17]        queue = [add(11), add(13), add(17), v1]</pre>

<pre>return:    1</pre>

step 5: call `remove(11)`

<pre>list = [13, 17]        queue = [add(11), add(13), add(17), v1, remove(11)]</pre>

step 6: call `snapshot()`

<pre>list = [13, 17]        queue = [add(11), add(13), add(17), v1, remove(11), v2]</pre>

<pre>return:    2</pre>

step 7: call `getAtVersion(1)`

It iterates over the queue members until v1 and will make another list according to that. The new list will be returned as the snapshot 1

<pre>list = [13, 17]        queue = [add(11), add(13), add(17), v1, remove(11), v2]      listV1 = [11, 13, 17]</pre>

<pre>return:     listV1[0] = 11</pre>