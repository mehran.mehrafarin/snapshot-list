package com.cyan.snapshot_list;

public class VersionOutOfBoundsException extends RuntimeException {

    /**
     * Constructs an <code>VersionOutFoBoundsException</code> with the
     * specified detail message.
     *
     * @param s the detail message.
     */
    public VersionOutOfBoundsException(String s) {
        super(s);
    }

}
