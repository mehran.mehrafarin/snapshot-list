package com.cyan.snapshot_list;


import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class SynchronizedUnbalancedSnapshotListMemento<E> implements SnapshotList<E> {

    private int version;
    private int tailVersion;
    private List<E> list;
    private Map<Integer, List<E>> map;
    private Supplier<? extends List<E>> supplier;

    public SynchronizedUnbalancedSnapshotListMemento(Supplier<? extends List<E>> supplier) {
        this.supplier = supplier;
        this.list = supplier.get();
        this.map = new TreeMap<>();
        this.tailVersion = 1;
    }


    @Override
    public synchronized void dropPriorSnapshots(int version) {

        versionCheck(version);
        tailVersion = version;

        for (int v = 1 ; v < version ; v++) {
            map.remove(v);
        }
    }

    @Override
    public synchronized E getAtVersion(int index, int version) {
        return getVersion(version).get(index);
    }

    @Override
    public synchronized int snapshot() {
        version++;
        List<E> other = supplier.get();
        other.addAll(list);
        this.map.put(version, other);
        return version;
    }

    @Override
    public synchronized int version() {
        return version;
    }

    private void versionCheck(int version) {

        if (version <= 0)
            throw new VersionOutOfBoundsException("Input version value must not be zero or negative number");

        if (version < tailVersion || version > this.version)
            throw new VersionOutOfBoundsException(outOfBoundsMsg(version));
    }

    private String outOfBoundsMsg(int Version) {
        return "Input version: " + Version + ", It must be between: " + this.tailVersion + " and: " + version;
    }

    public List<E> getVersion(int version) {
        versionCheck(version);
        return map.get(version);
    }

    public synchronized int size() {
        return list.size();
    }

    public synchronized boolean isEmpty() {
        return list.isEmpty();
    }

    public synchronized boolean contains(Object o) {
        return list.contains(o);
    }

    public synchronized int indexOf(Object o) {
        return list.indexOf(o);
    }

    public synchronized int lastIndexOf(Object o) {
        return list.lastIndexOf(o);
    }

    public synchronized Object[] toArray() {
        return list.toArray();
    }

    public synchronized <T> T[] toArray(T[] a) {
        return list.toArray(a);
    }

    public synchronized E get(int index) {
        return list.get(index);
    }

    public synchronized E set(int index, E element) {
        return list.set(index, element);
    }

    public synchronized boolean add(E e) {
        return list.add(e);
    }

    public synchronized void add(int index, E element) {
        list.add(index, element);
    }

    public synchronized E remove(int index) {
        return list.remove(index);
    }

    public synchronized boolean remove(Object o) {
        return list.remove(o);
    }

    public synchronized void clear() {
        list.clear();
    }

    public synchronized boolean addAll(Collection<? extends E> c) {
        return list.addAll(c);
    }

    public synchronized boolean addAll(int index, Collection<? extends E> c) {
        return list.addAll(index, c);
    }

    public synchronized boolean removeAll(Collection<?> c) {
        return list.removeAll(c);
    }

    public synchronized boolean retainAll(Collection<?> c) {
        return list.retainAll(c);
    }

    public synchronized ListIterator<E> listIterator(int index) {
        return list.listIterator(index);
    }

    public synchronized ListIterator<E> listIterator() {
        return list.listIterator();
    }

    public synchronized Iterator<E> iterator() {
        return list.iterator();
    }

    public synchronized List<E> subList(int fromIndex, int toIndex) {
        return list.subList(fromIndex, toIndex);
    }

    public synchronized void forEach(Consumer<? super E> action) {
        list.forEach(action);
    }

    public Spliterator<E> spliterator() {
        return list.spliterator();
    }

    public synchronized boolean removeIf(Predicate<? super E> filter) {
        return list.removeIf(filter);
    }

    public synchronized void replaceAll(UnaryOperator<E> operator) {
        list.replaceAll(operator);
    }

    public synchronized void sort(Comparator<? super E> c) {
        list.sort(c);
    }

    public synchronized boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }

    public Stream<E> stream() {
        return list.stream();
    }

    public Stream<E> parallelStream() {
        return list.parallelStream();
    }

    public synchronized boolean equals(Object o) {
        return list.equals(o);
    }

    public synchronized int hashCode() {
        return list.hashCode();
    }

    public String toString() {
        return list.toString();
    }

}
