package com.cyan.snapshot_list;


import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class SynchronizedUnbalancedSnapshotList<E> implements SnapshotList<E> {

    private int version;
    private int tailVersion;
    private List<E> list;
    private List<E> other;
    private LinkedList<Command> commands;
    private Supplier<? extends List<E>> supplier;

    public SynchronizedUnbalancedSnapshotList(Supplier<? extends List<E>> supplier) {
        this.list = supplier.get();
        this.other = supplier.get();
        this.commands = new LinkedList<>();
        this.tailVersion = 1;
        this.supplier = supplier;
    }

    interface Command {
        void execute();

        default int getVersion() {
            return -1;
        }
    }

    class VersionCommand implements Command {

        private int version;

        public VersionCommand(int version) {
            this.version = version;
        }

        @Override
        public void execute() {
        }

        @Override
        public int getVersion() {
            return version;
        }
    }

    @Override
    public synchronized void dropPriorSnapshots(int version) {

        versionCheck(version);
        tailVersion = version;
    }

    @Override
    public synchronized E getAtVersion(int index, int version) {
        return getVersion(version).get(index);
    }

    @Override
    public synchronized int snapshot() {
        version++;
        commands.offer(new VersionCommand(version));
        return version;
    }

    @Override
    public synchronized int version() {
        return version;
    }

    public synchronized List<E> getVersion(int version) {
        versionCheck(version);
        other.clear();
        for (Command command : commands) {
            if (command.getVersion() == version)
                break;
            command.execute();
        }
        return other;
    }

    private void versionCheck(int version) {

        if (version <= 0)
            throw new VersionOutOfBoundsException("Input version value must not be zero or negative number");

        if (version < tailVersion || version > this.version)
            throw new VersionOutOfBoundsException(outOfBoundsMsg(version));
    }

    private String outOfBoundsMsg(int Version) {
        return "Input version: " + Version + ", It must be between: " + this.tailVersion + " and: " + version;
    }

    public synchronized int size() {
        return list.size();
    }

    public synchronized boolean isEmpty() {
        return list.isEmpty();
    }

    public synchronized boolean contains(Object o) {
        return list.contains(o);
    }

    public synchronized int indexOf(Object o) {
        return list.indexOf(o);
    }

    public synchronized int lastIndexOf(Object o) {
        return list.lastIndexOf(o);
    }

    public synchronized Object[] toArray() {
        return list.toArray();
    }

    public synchronized <T> T[] toArray(T[] a) {
        return list.toArray(a);
    }

    public synchronized E get(int index) {
        return list.get(index);
    }

    public synchronized E set(int index, E element) {
        commands.add(() -> other.set(index, element));
        return list.set(index, element);
    }

    public synchronized boolean add(E e) {
        commands.add(() -> other.add(e));
        return list.add(e);
    }

    public synchronized void add(int index, E element) {
        commands.add(() -> other.add(index, element));
        list.add(index, element);
    }

    public synchronized E remove(int index) {
        commands.add(() -> other.remove(index));
        return list.remove(index);
    }

    public synchronized boolean remove(Object o) {
        commands.add(() -> other.remove(o));
        return list.remove(o);
    }

    public synchronized void clear() {
        commands.add(() -> other.clear());
        list.clear();
    }

    public synchronized boolean addAll(Collection<? extends E> c) {
        List<E> l = supplier.get();
        l.addAll(c);
        commands.add(() -> other.addAll(l));
        return list.addAll(c);
    }

    public synchronized boolean addAll(int index, Collection<? extends E> c) {
        List<E> l = supplier.get();
        l.addAll(c);
        commands.add(() -> other.addAll(index, l));
        return list.addAll(index, c);
    }

    public synchronized boolean removeAll(Collection<?> c) {
        List<E> l = supplier.get();
        l.addAll((Collection<? extends E>) c);
        commands.add(() -> other.removeAll(l));
        return list.removeAll(c);
    }

    public synchronized boolean retainAll(Collection<?> c) {
        List<E> l = supplier.get();
        l.addAll((Collection<? extends E>) c);
        commands.add(() -> other.retainAll(l));
        return list.retainAll(c);
    }

    public synchronized ListIterator<E> listIterator(int index) {
        return list.listIterator(index);
    }

    public synchronized ListIterator<E> listIterator() {
        return list.listIterator();
    }

    public synchronized Iterator<E> iterator() {
        return list.iterator();
    }

    public synchronized List<E> subList(int fromIndex, int toIndex) {
        return list.subList(fromIndex, toIndex);
    }

    public synchronized void forEach(Consumer<? super E> action) {
        list.forEach(action);
    }

    public Spliterator<E> spliterator() {
        return list.spliterator();
    }

    public synchronized boolean removeIf(Predicate<? super E> filter) {
        commands.add(() -> other.removeIf(filter));
        return list.removeIf(filter);
    }

    public synchronized void replaceAll(UnaryOperator<E> operator) {
        commands.add(() -> other.replaceAll(operator));
        list.replaceAll(operator);
    }

    public synchronized void sort(Comparator<? super E> c) {
        commands.add(() -> other.sort(c));
        list.sort(c);
    }

    public synchronized boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }

    public Stream<E> stream() {
        return list.stream();
    }

    public Stream<E> parallelStream() {
        return list.parallelStream();
    }

    public synchronized boolean equals(Object o) {
        return list.equals(o);
    }

    public synchronized int hashCode() {
        return list.hashCode();
    }

    public String toString() {
        return list.toString();
    }

}
