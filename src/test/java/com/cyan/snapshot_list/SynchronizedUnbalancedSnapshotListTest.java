package com.cyan.snapshot_list;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class SynchronizedUnbalancedSnapshotListTest {

    private SynchronizedUnbalancedSnapshotList<Integer> list;

    @BeforeEach
    public void setup() {
        this.list = new SynchronizedUnbalancedSnapshotList<>(ArrayList::new);
    }

    @Test
    void version_AfterInstantiation_ReturnZero() {
        int expected = 0;
        int actual = list.version();
        assertEquals(expected, actual);
    }

    @Test
    void snapshot_AfterInstantiation_ReturnOne() {
        int expected = 1;
        int actual = list.snapshot();
        assertEquals(expected, actual);
    }

    @Test
    void getAtVersion_VersionEqualZeroOrNegativeNumber_ThrowException() {
        assertThrows(VersionOutOfBoundsException.class, () -> this.list.getAtVersion(0, 0));
        assertThrows(VersionOutOfBoundsException.class, () -> this.list.getAtVersion(0, -1));
    }

    @Test
    void getAtVersion_VersionEqualOneAfterInstantiation_ThrowException() {
        assertThrows(VersionOutOfBoundsException.class, () -> this.list.getAtVersion(0, 1));
    }

    @Test
    void dropPriorSnapshots_VersionEqualZeroOrNegativeNumber_ThrowException() {
        assertThrows(VersionOutOfBoundsException.class, () -> this.list.dropPriorSnapshots(0));
        assertThrows(VersionOutOfBoundsException.class, () -> this.list.dropPriorSnapshots(-1));
    }

    @Test
    void dropPriorSnapshots_VersionEqualOneAfterInstantiation_ThrowException() {
        assertThrows(VersionOutOfBoundsException.class, () -> this.list.dropPriorSnapshots(1));
    }

    @Test
    void testVersion() {
        list.add(1);
        list.add(2);
        list.add(3);

        list.snapshot();

        list.clear();

        int expected = 1;
        int actual = list.version();
        assertEquals(expected, actual);
    }

    @Test
    void testSnapShot() {

        list.add(1);
        list.add(2);
        list.add(3);

        int firstVersion = list.snapshot();

        list.clear();

        List<Integer> expected = Arrays.asList(1, 2, 3);
        List<Integer> actual = list.getVersion(firstVersion);
        assertIterableEquals(expected, actual);
    }

    @Test
    void testGetVersion() {

        list.add(1);
        list.add(2);
        list.add(3);

        int firstVersion = list.snapshot();

        list.clear();

        List<Integer> expected = Arrays.asList(1, 2, 3);
        List<Integer> actual = list.getVersion(firstVersion);
        assertIterableEquals(expected, actual);
    }

    @Test
    void getAtVersion() {

        list.add(1);
        list.add(2);
        list.add(3);

        int firstVersion = list.snapshot();

        list.clear();

        Integer expected = 3;
        Integer actual = list.getAtVersion(2, firstVersion);
        assertEquals(expected, actual);
    }

    @Test
    void testDropPriorSnapshots() {

        list.add(1);
        list.add(2);
        list.add(3);

        list.snapshot();
        list.remove(new Integer(1));

        int secondVersion = list.snapshot();

        list.add(4);
        list.dropPriorSnapshots(secondVersion);

        assertThrows(VersionOutOfBoundsException.class, () -> this.list.dropPriorSnapshots(1));

        List<Integer> expected = Arrays.asList(2, 3);
        List<Integer> actual = list.getVersion(secondVersion);
        assertIterableEquals(expected, actual);
    }

    @Test
    void testSet() {

        list.add(1);
        list.set(0, 10);

        int firstVersion = list.snapshot();

        list.clear();

        List<Integer> expected = Collections.singletonList(10);
        List<Integer> actual = list.getVersion(firstVersion);
        assertIterableEquals(expected, actual);
    }

    @Test
    void testAdd() {

        list.add(1);
        list.add(1, 2);

        int firstVersion = list.snapshot();

        list.clear();

        List<Integer> expected = Arrays.asList(1, 2);
        List<Integer> actual = list.getVersion(firstVersion);
        assertIterableEquals(expected, actual);
    }

    @Test
    void testRemove() {

        list.add(1);
        list.add(2);
        list.add(3);
        list.remove(1);
        list.remove(Integer.valueOf(3));

        int firstVersion = list.snapshot();

        list.clear();

        List<Integer> expected = Collections.singletonList(1);
        List<Integer> actual = list.getVersion(firstVersion);
        assertIterableEquals(expected, actual);

    }

    @Test
    void testClear() {

        list.add(1);
        list.clear();

        int firstVersion = list.snapshot();

        list.add(1);

        List<Integer> expected = Collections.emptyList();
        List<Integer> actual = list.getVersion(firstVersion);
        assertIterableEquals(expected, actual);
    }

    @Test
    void testAddAll() {

        List<Integer> integers = new ArrayList<>();
        integers.add(3);
        integers.add(4);

        list.addAll(Arrays.asList(1, 2));
        list.addAll(2, integers);

        integers.clear();

        int firstVersion = list.snapshot();

        list.clear();

        List<Integer> expected = Arrays.asList(1, 2, 3, 4);
        List<Integer> actual = list.getVersion(firstVersion);
        assertIterableEquals(expected, actual);
    }

    @Test
    void testRemoveAll() {

        list.add(1);
        list.removeAll(Collections.singletonList(1));

        int firstVersion = list.snapshot();

        list.add(1);

        List<Integer> expected = Collections.emptyList();
        List<Integer> actual = list.getVersion(firstVersion);
        assertIterableEquals(expected, actual);
    }

    @Test
    void testRetainAll() {

        list.add(1);
        list.add(2);
        list.retainAll(Collections.singletonList(1));

        int firstVersion = list.snapshot();

        list.add(2);

        List<Integer> expected = Collections.singletonList(1);
        List<Integer> actual = list.getVersion(firstVersion);
        assertIterableEquals(expected, actual);
    }

    @Test
    void testRemoveIf() {

        list.add(1);
        list.add(2);
        list.removeIf((e) -> e % 2 == 0);

        int firstVersion = list.snapshot();

        list.clear();

        List<Integer> expected = Collections.singletonList(1);
        List<Integer> actual = list.getVersion(firstVersion);
        assertIterableEquals(expected, actual);
    }

    @Test
    void testSort() {

        list.add(2);
        list.add(1);
        list.sort(Integer::compareTo);

        int firstVersion = list.snapshot();

        list.clear();

        List<Integer> expected = Arrays.asList(1, 2);
        List<Integer> actual = list.getVersion(firstVersion);
        assertIterableEquals(expected, actual);

    }
}